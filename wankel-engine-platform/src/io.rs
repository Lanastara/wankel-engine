use async_trait::async_trait;

#[async_trait(?Send)]
pub trait IOContext {
    async fn open_file(&self, path: &str) -> Option<Vec<u8>>;
}
