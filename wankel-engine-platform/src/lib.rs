pub use tracing;

pub mod geom;
pub mod io;
pub mod net;

pub mod prelude {
    pub use crate::geom::*;
    pub use crate::Config;
}

#[derive(Debug)]
pub struct Error(String);

impl Error {
    pub fn new(message: &str) -> Self {
        Self(message.to_string())
    }
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        writeln!(f, "{}", self.0)
    }
}

impl std::error::Error for Error {}

pub struct Config {
    pub size: geom::Size<u32>,
    pub title: String,
}
