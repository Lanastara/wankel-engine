pub enum WebsocketResult {
    Message(Message),
    Error(crate::Error),
    Disconnect,
}

pub enum Message {
    String(String),
    Binary(Vec<u8>),
}
