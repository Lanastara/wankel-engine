use std::mem::size_of;

use bytemuck::{Pod, Zeroable};
use image::DynamicImage;
use wankel_engine_macros::Buffer;
use wankel_engine_platform::{
    prelude::{Point, Rect, Size},
    tracing::{debug, trace, warn},
};
use wgpu::{util::DeviceExt, Color};

use crate::{
    texture::{atlas::TextureAtlas, Texture},
    Buffer, BufferElement, Handle, NineSlice, PrepareContext, RenderContext, TextureOptions,
    TextureRotate, ToAny,
};

use super::{InitContext, Layer, LayerConstructor};

pub mod prelude {
    pub use super::{Atlas, TextureAtlasLayer};
}

impl<T: 'static> ToAny for T
where
    T: Layer,
{
    fn to_any(&self) -> &dyn std::any::Any {
        self
    }

    fn to_any_mut(&mut self) -> &mut dyn std::any::Any {
        self
    }
}

#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq, Pod, Zeroable, Buffer)]
pub struct Vertex {
    pub tex_coords: [f32; 2],
}

#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq, Pod, Zeroable, Buffer)]
#[buffer(instance)]
pub struct Instance {
    #[buffer(location = 1)]
    pub position: [f32; 3],
    pub size: [f32; 2],
    pub texture_position: [f32; 2],
    pub texture_size: [f32; 2],
    pub color: [f32; 4],
    pub transform: [[f32; 2]; 2],
}

#[derive(Debug)]
pub struct LayerTexture {
    handle: Handle<DynamicImage>,
    screen_position: Point<f32>,
    screen_size: Size<f32>,
    options: TextureOptions,
}

impl LayerTexture {
    pub fn disable(&mut self) {
        self.options.disabled = true;
    }

    pub fn enable(&mut self) {
        self.options.disabled = false;
    }

    pub fn set_tint_color(&mut self, color: Option<Color>) {
        self.options.tint_color = color;
    }

    pub fn set_flip_h(&mut self, flip_h: bool) {
        self.options.flip_h = flip_h;
    }

    pub fn set_flip_v(&mut self, flip_v: bool) {
        self.options.flip_v = flip_v;
    }

    pub fn set_rotation(&mut self, rotation: TextureRotate) {
        self.options.rotate = rotation;
    }

    pub fn set_position(&mut self, position: Point<f32>) {
        self.screen_position = position;
    }

    pub fn set_size(&mut self, size: Size<f32>) {
        self.screen_size = size;
    }
}

impl Handle<LayerTexture, (bool, usize)> {
    pub fn update<TOut, TFn: FnMut(&mut LayerTexture) -> TOut>(
        &self,
        layer: &mut TextureAtlasLayer,
        mut update: TFn,
    ) -> Option<TOut> {
        let texture = match self.0 {
            (true, i) => layer.translucents.get_mut(i),
            (false, i) => layer.solids.get_mut(i),
        }?;

        layer.has_changed = true;

        Some(update(texture))
    }
}

pub struct Atlas;

#[derive(Debug)]
pub struct TextureAtlasLayer {
    atlas: Handle<Atlas>,
    solids: Vec<LayerTexture>,
    translucents: Vec<LayerTexture>,
    solid_pipeline: wgpu::RenderPipeline,
    translucent_pipeline: wgpu::RenderPipeline,
    solid_instance_buffer: wgpu::Buffer,
    translucent_instance_buffer: wgpu::Buffer,
    texture_bind_group: BindGroup,
    projection_bind_group: wgpu::BindGroup,
    projection_buffer: wgpu::Buffer,
    vertex_buffer: wgpu::Buffer,
    removed_solids: Vec<usize>,
    removed_translucents: Vec<usize>,
    num_solids: usize,
    num_translucents: usize,
    has_changed: bool,
}

impl LayerConstructor for TextureAtlasLayer {
    type Params = Handle<Atlas>;

    fn new(context: InitContext, tex: Self::Params) -> Self {
        let InitContext {
            device,
            size,
            config,
        } = context;

        let view_projection: [[f32; 4]; 4] = [
            [2.0 / size.width, 0.0, 0.0, 0.0],
            [0.0, -2.0 / size.height, 0.0, 0.0],
            [0.0, 0.0, 0.1, 0.0],
            [-1.0, 1.0, 0.0, 1.0],
        ];

        let projection_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("TextureAtlasLayer View Projection"),
            usage: wgpu::BufferUsages::UNIFORM | wgpu::BufferUsages::COPY_DST,
            contents: bytemuck::cast_slice(&[view_projection]),
        });

        let texture_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[
                    wgpu::BindGroupLayoutEntry {
                        binding: 0,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Texture {
                            sample_type: wgpu::TextureSampleType::Float { filterable: true },
                            view_dimension: wgpu::TextureViewDimension::D2,
                            multisampled: false,
                        },
                        count: None,
                    },
                    wgpu::BindGroupLayoutEntry {
                        binding: 1,
                        visibility: wgpu::ShaderStages::FRAGMENT,
                        ty: wgpu::BindingType::Sampler(wgpu::SamplerBindingType::Filtering),
                        count: None,
                    },
                ],
                label: Some("TextureAtlasLayer bind group layout"),
            });

        let projection_bind_group_layout =
            device.create_bind_group_layout(&wgpu::BindGroupLayoutDescriptor {
                entries: &[wgpu::BindGroupLayoutEntry {
                    binding: 0,
                    visibility: wgpu::ShaderStages::VERTEX,
                    ty: wgpu::BindingType::Buffer {
                        ty: wgpu::BufferBindingType::Uniform,
                        has_dynamic_offset: false,
                        min_binding_size: None,
                    },
                    count: None,
                }],
                label: Some("TextureAtlasLayer Projection bind_group_layout"),
            });

        let projection_bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
            layout: &projection_bind_group_layout,
            entries: &[wgpu::BindGroupEntry {
                binding: 0,
                resource: projection_buffer.as_entire_binding(),
            }],
            label: Some("TextureAtlasLayer Projection bind_group"),
        });

        let shader = device.create_shader_module(wgpu::ShaderModuleDescriptor {
            label: Some("TextureAtlasLayer Shader"),
            source: wgpu::ShaderSource::Wgsl(include_str!("texture.wgsl").into()),
        });

        let render_pipeline_layout =
            device.create_pipeline_layout(&wgpu::PipelineLayoutDescriptor {
                label: Some("TextureAtlasLayer Render Pipeline Layout"),
                bind_group_layouts: &[&texture_bind_group_layout, &projection_bind_group_layout],
                push_constant_ranges: &[],
            });

        let solid_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("TextureAtlasLayer Render Pipeline"),
            layout: Some(&render_pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shader,
                entry_point: "vs_main",
                buffers: &[Vertex::desc(), Instance::desc()],
            },
            fragment: Some(wgpu::FragmentState {
                module: &shader,
                entry_point: "fs_main",
                targets: &[Some(wgpu::ColorTargetState {
                    format: config.format.clone(),
                    blend: Some(wgpu::BlendState::REPLACE),
                    write_mask: wgpu::ColorWrites::ALL,
                })],
            }),
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleStrip,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: Some(wgpu::Face::Back),
                polygon_mode: wgpu::PolygonMode::Fill,
                unclipped_depth: false,
                conservative: false,
            },
            depth_stencil: Some(wgpu::DepthStencilState {
                format: Texture::DEPTH_FORMAT,
                depth_write_enabled: true,
                depth_compare: wgpu::CompareFunction::Less,
                stencil: wgpu::StencilState::default(),
                bias: wgpu::DepthBiasState::default(),
            }),
            multisample: wgpu::MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
            multiview: None,
        });

        let translucent_pipeline = device.create_render_pipeline(&wgpu::RenderPipelineDescriptor {
            label: Some("TextureAtlasLayer Render Pipeline"),
            layout: Some(&render_pipeline_layout),
            vertex: wgpu::VertexState {
                module: &shader,
                entry_point: "vs_main",
                buffers: &[Vertex::desc(), Instance::desc()],
            },
            fragment: Some(wgpu::FragmentState {
                module: &shader,
                entry_point: "fs_main",
                targets: &[Some(wgpu::ColorTargetState {
                    format: config.format.clone(),
                    blend: Some(wgpu::BlendState::ALPHA_BLENDING),
                    write_mask: wgpu::ColorWrites::ALL,
                })],
            }),
            primitive: wgpu::PrimitiveState {
                topology: wgpu::PrimitiveTopology::TriangleStrip,
                strip_index_format: None,
                front_face: wgpu::FrontFace::Ccw,
                cull_mode: Some(wgpu::Face::Back),
                polygon_mode: wgpu::PolygonMode::Fill,
                unclipped_depth: false,
                conservative: false,
            },
            depth_stencil: Some(wgpu::DepthStencilState {
                format: Texture::DEPTH_FORMAT,
                depth_write_enabled: false,
                depth_compare: wgpu::CompareFunction::Less,
                stencil: wgpu::StencilState::default(),
                bias: wgpu::DepthBiasState::default(),
            }),
            multisample: wgpu::MultisampleState {
                count: 1,
                mask: !0,
                alpha_to_coverage_enabled: false,
            },
            multiview: None,
        });

        let vertices = vec![
            Vertex {
                tex_coords: [0.0, 1.0],
            },
            Vertex {
                tex_coords: [1.0, 1.0],
            },
            Vertex {
                tex_coords: [0.0, 0.0],
            },
            Vertex {
                tex_coords: [1.0, 0.0],
            },
        ];

        let vertex_buffer = device.create_buffer_init(&wgpu::util::BufferInitDescriptor {
            label: Some("TextureAtlasLAyer VertexBuffer"),
            contents: bytemuck::cast_slice(&vertices),
            usage: wgpu::BufferUsages::VERTEX | wgpu::BufferUsages::COPY_DST,
        });

        let size = 100 * size_of::<Instance>() as u64;
        debug!("Creating new Buffers with size: {}", size);
        let solid_instance_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("TextureAtlasLayer SolidInstanceBuffer"),

            usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::VERTEX,
            size,
            mapped_at_creation: false,
        });
        let translucent_instance_buffer = device.create_buffer(&wgpu::BufferDescriptor {
            label: Some("TextureAtlasLayer TranslucentInstanceBuffer"),

            usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::VERTEX,
            size,
            mapped_at_creation: false,
        });

        Self {
            atlas: tex,
            solids: Vec::new(),
            translucents: Vec::new(),
            solid_pipeline,
            translucent_pipeline,
            solid_instance_buffer,
            translucent_instance_buffer,
            texture_bind_group: BindGroup::Layout(texture_bind_group_layout),
            projection_buffer,
            projection_bind_group,
            vertex_buffer,
            has_changed: true,
            removed_solids: Vec::new(),
            removed_translucents: Vec::new(),
            num_solids: 0,
            num_translucents: 0,
        }
    }
}

#[derive(Debug, Clone)]
pub struct NineSliceHandle([[Handle<LayerTexture, (bool, usize)>; 3]; 3]);

impl TextureAtlasLayer {
    pub fn render_texture(
        &mut self,
        texture: Handle<DynamicImage>,
        screen_area: Rect<f32>,
        options: impl Into<Option<TextureOptions>>,
    ) -> Handle<LayerTexture, (bool, usize)> {
        let (screen_position, screen_size) = screen_area.into_parts();

        let options: TextureOptions = options.into().unwrap_or_default();

        self.has_changed = true;

        match options.specific {
            crate::SpecificTextureOptions::Solid(_) => {
                let i = self.solids.len();
                self.solids.push(LayerTexture {
                    screen_position,
                    screen_size,
                    handle: texture,
                    options,
                });
                Handle::<LayerTexture, (bool, usize)>::new((false, i))
            }
            crate::SpecificTextureOptions::Translucent(_) => {
                let i = self.translucents.len();
                self.translucents.push(LayerTexture {
                    screen_position,
                    screen_size,
                    handle: texture,
                    options,
                });
                Handle::new((true, i))
            }
        }
    }

    pub fn render_nine_slice(
        &mut self,
        nine_slice: &NineSlice,
        screen_area: Rect<f32>,
        options: impl Into<Option<TextureOptions>>,
    ) -> NineSliceHandle {
        let options = options.into();

        NineSliceHandle([
            [
                self.render_texture(
                    nine_slice.textures[0][0],
                    Point::new(screen_area.x, screen_area.y)
                        + Size::new(nine_slice.border_width, nine_slice.border_width),
                    options.clone(),
                ),
                self.render_texture(
                    nine_slice.textures[0][1],
                    Point::new(screen_area.x + nine_slice.border_width, screen_area.y)
                        + Size::new(
                            screen_area.width - 2.0 * nine_slice.border_width,
                            nine_slice.border_width,
                        ),
                    options.clone(),
                ),
                self.render_texture(
                    nine_slice.textures[0][2],
                    Point::new(
                        screen_area.x + screen_area.width - nine_slice.border_width,
                        screen_area.y,
                    ) + Size::new(nine_slice.border_width, nine_slice.border_width),
                    options.clone(),
                ),
            ],
            [
                self.render_texture(
                    nine_slice.textures[1][0],
                    Point::new(screen_area.x, screen_area.y + nine_slice.border_width)
                        + Size::new(
                            nine_slice.border_width,
                            screen_area.height - 2.0 * nine_slice.border_width,
                        ),
                    options.clone(),
                ),
                self.render_texture(
                    nine_slice.textures[1][1],
                    Point::new(
                        screen_area.x + nine_slice.border_width,
                        screen_area.y + nine_slice.border_width,
                    ) + Size::new(
                        screen_area.width - 2.0 * nine_slice.border_width,
                        screen_area.height - 2.0 * nine_slice.border_width,
                    ),
                    options.clone(),
                ),
                self.render_texture(
                    nine_slice.textures[1][2],
                    Point::new(
                        screen_area.x + screen_area.width - nine_slice.border_width,
                        screen_area.y + nine_slice.border_width,
                    ) + Size::new(
                        nine_slice.border_width,
                        screen_area.height - 2.0 * nine_slice.border_width,
                    ),
                    options.clone(),
                ),
            ],
            [
                self.render_texture(
                    nine_slice.textures[2][0],
                    Point::new(
                        screen_area.x,
                        screen_area.y + screen_area.height - nine_slice.border_width,
                    ) + Size::new(nine_slice.border_width, nine_slice.border_width),
                    options.clone(),
                ),
                self.render_texture(
                    nine_slice.textures[2][1],
                    Point::new(
                        screen_area.x + nine_slice.border_width,
                        screen_area.y + screen_area.height - nine_slice.border_width,
                    ) + Size::new(
                        screen_area.width - 2.0 * nine_slice.border_width,
                        nine_slice.border_width,
                    ),
                    options.clone(),
                ),
                self.render_texture(
                    nine_slice.textures[2][2],
                    Point::new(
                        screen_area.x + screen_area.width - nine_slice.border_width,
                        screen_area.y + screen_area.height - nine_slice.border_width,
                    ) + Size::new(nine_slice.border_width, nine_slice.border_width),
                    options.clone(),
                ),
            ],
        ])
    }
}

impl TextureAtlasLayer {
    pub fn clear(&mut self) {
        self.solids.clear();
        self.has_changed = true;
    }

    pub(crate) fn get_translucent_instances(
        &self,
        atlas: &Box<dyn TextureAtlas>,
        depth: f32,
    ) -> Vec<Instance> {
        self.translucents
            .iter()
            .filter_map(|t| self.instance_from_texture(atlas, t, depth))
            .collect()
    }

    pub(crate) fn get_solid_instances(
        &self,
        atlas: &Box<dyn TextureAtlas>,
        depth: f32,
    ) -> Vec<Instance> {
        self.solids
            .iter()
            .filter_map(|t| self.instance_from_texture(atlas, t, depth))
            .collect()
    }

    fn instance_from_texture(
        &self,
        atlas: &Box<dyn TextureAtlas>,
        t: &LayerTexture,
        depth: f32,
    ) -> Option<Instance> {
        atlas.get_loaded_texture(t.handle).and_then(|texture_rect| {
            let (texture_position, texture_size) = texture_rect.as_f32().into_parts();

            let options = t.options.clone();

            if options.disabled {
                return None;
            }

            let pos = [t.screen_position.x, t.screen_position.y, depth];

            Some(Instance {
                position: pos,

                size: t.screen_size.into_raw(),
                texture_position: texture_position.into_raw(),
                texture_size: texture_size.into_raw(),
                color: options.get_color(),
                transform: options.get_transform(),
            })
        })
    }
}

impl Layer for TextureAtlasLayer {
    fn prepare(&mut self, context: PrepareContext) {
        let PrepareContext {
            texture_atlases,
            device,
            queue,
            textures,
            size,
            depth,
            ..
        } = context;

        let view_projection: [[f32; 4]; 4] = [
            [2.0 / size.width, 0.0, 0.0, 0.0],
            [0.0, -2.0 / size.height, 0.0, 0.0],
            [0.0, 0.0, 1.0, 0.0],
            [-1.0, 1.0, 0.0, 1.0],
        ];

        queue.write_buffer(
            &self.projection_buffer,
            0,
            bytemuck::cast_slice(&[view_projection]),
        );

        if self.has_changed {
            if let Some(atlas) = texture_atlases.get(&self.atlas) {
                let texture = atlas.get_texture();

                let solid_instances = self.get_solid_instances(atlas, depth);

                if !solid_instances.is_empty() {
                    let chunk: &[u8] = bytemuck::cast_slice(&solid_instances);

                    if self.solid_instance_buffer.size() < chunk.len() as wgpu::BufferAddress {
                        let size = ((solid_instances.len() as u64 / 100) + 1)
                            * 100
                            * size_of::<Instance>() as u64;

                        debug!("Creating new SolidBuffer with size: {}", size);

                        self.solid_instance_buffer =
                            device.create_buffer(&wgpu::BufferDescriptor {
                                label: Some("TextureAtlasLayer SolidInstanceBuffer"),
                                mapped_at_creation: false,
                                size: size,
                                usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::VERTEX,
                            });
                    }
                    queue.write_buffer(&self.solid_instance_buffer, 0, chunk);
                } else {
                    debug!("No solids");
                }

                let translucent_instances = self.get_translucent_instances(atlas, depth);

                if !translucent_instances.is_empty() {
                    let chunk: &[u8] = bytemuck::cast_slice(&translucent_instances);

                    if self.translucent_instance_buffer.size() < chunk.len() as wgpu::BufferAddress
                    {
                        let size = ((translucent_instances.len() as u64 / 100) + 1)
                            * 100
                            * size_of::<Instance>() as u64;

                        debug!("Creating new TranslucentBuffer with size: {}", size);

                        self.translucent_instance_buffer =
                            device.create_buffer(&wgpu::BufferDescriptor {
                                label: Some("TextureAtlasLayer TranslucentInstanceBuffer"),
                                mapped_at_creation: false,
                                size: size,
                                usage: wgpu::BufferUsages::COPY_DST | wgpu::BufferUsages::VERTEX,
                            });
                    }
                    queue.write_buffer(&self.translucent_instance_buffer, 0, chunk);
                }

                if let Some(texture) = textures.get(&texture) {
                    if let BindGroup::Layout(layout) = &self.texture_bind_group {
                        let bind_group = device.create_bind_group(&wgpu::BindGroupDescriptor {
                            layout: &layout,
                            entries: &[
                                wgpu::BindGroupEntry {
                                    binding: 0,
                                    resource: wgpu::BindingResource::TextureView(&texture.view),
                                },
                                wgpu::BindGroupEntry {
                                    binding: 1,
                                    resource: wgpu::BindingResource::Sampler(&texture.sampler),
                                },
                            ],
                            label: Some("TextureAtlasLayer BindGroup"),
                        });

                        self.texture_bind_group = BindGroup::Initialized(bind_group);
                    }
                }
            }
        }
    }

    fn render_solids(&mut self, mut context: RenderContext) -> Result<(), wgpu::SurfaceError> {
        let RenderContext {
            ref mut encoder,
            view,
            depth_texture,
            ..
        } = context;

        if let BindGroup::Initialized(bind_group) = &self.texture_bind_group {
            if !self.solids.is_empty() {
                let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                    label: Some("Render Pass"),
                    color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                        view: &view,
                        resolve_target: None,
                        ops: wgpu::Operations {
                            load: wgpu::LoadOp::Load,
                            store: true,
                        },
                    })],
                    depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                        view: &depth_texture.view,
                        depth_ops: Some(wgpu::Operations {
                            load: wgpu::LoadOp::Load,
                            store: true,
                        }),
                        stencil_ops: None,
                    }),
                });

                render_pass.set_pipeline(&self.solid_pipeline);

                let solid_count = self.solids.len() as u32;

                render_pass.set_bind_group(0, &bind_group, &[]);
                render_pass.set_bind_group(1, &self.projection_bind_group, &[]);

                render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));

                render_pass.set_vertex_buffer(1, self.solid_instance_buffer.slice(..));

                // trace!("Drawing {} solids", solid_count);
                render_pass.draw(0..4, 0..solid_count);
            } else {
                debug!("No Solids to render");
            }

            self.has_changed = false;
        } else {
            warn!("TextureAtlasLayer not initialized");
        }

        Ok(())
    }

    fn render_translucents(&mut self, context: RenderContext) -> Result<(), wgpu::SurfaceError> {
        let RenderContext {
            encoder,
            view,
            depth_texture,
            ..
        } = context;

        if let BindGroup::Initialized(bind_group) = &self.texture_bind_group {
            if !self.translucents.is_empty() {
                let mut render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                    label: Some("Render Pass"),
                    color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                        view: &view,
                        resolve_target: None,
                        ops: wgpu::Operations {
                            load: wgpu::LoadOp::Load,
                            store: true,
                        },
                    })],
                    depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                        view: &depth_texture.view,
                        depth_ops: Some(wgpu::Operations {
                            load: wgpu::LoadOp::Load,
                            store: false,
                        }),
                        stencil_ops: None,
                    }),
                });

                render_pass.set_pipeline(&self.translucent_pipeline);

                let translucent_count = self.translucents.len() as u32;

                render_pass.set_bind_group(0, &bind_group, &[]);
                render_pass.set_bind_group(1, &self.projection_bind_group, &[]);

                render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));

                render_pass.set_bind_group(0, &bind_group, &[]);
                render_pass.set_bind_group(1, &self.projection_bind_group, &[]);

                render_pass.set_vertex_buffer(0, self.vertex_buffer.slice(..));

                render_pass.set_vertex_buffer(1, self.translucent_instance_buffer.slice(..));

                trace!("Drawing {} translucents", translucent_count);

                render_pass.draw(0..4, 0..translucent_count);
            }

            self.has_changed = false;
        }

        Ok(())
    }

    fn cleanup(&mut self) {}
}

#[derive(Debug)]
enum BindGroup {
    Layout(wgpu::BindGroupLayout),
    Initialized(wgpu::BindGroup),
}
