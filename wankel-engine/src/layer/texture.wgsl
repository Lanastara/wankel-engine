// Vertex shader

struct ProjectionUniform {
    view_proj: mat4x4<f32>,
};
@group(1) @binding(0)
var<uniform> projection: ProjectionUniform;

struct VertexInput {
    @location(0) tex_coords: vec2<f32>,
}

struct InstanceInput{
    @location(1) position: vec3<f32>,
    @location(2) size: vec2<f32>,
    @location(3) texture_position: vec2<f32>,
    @location(4) texture_size: vec2<f32>,
    @location(5) color: vec4<f32>,
    @location(6) transform: vec4<f32>,
}

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) tex_coords: vec2<f32>,
    @location(1) texture_position: vec2<f32>,
    @location(2) texture_size: vec2<f32>,
    @location(3) color: vec4<f32>,
}

@vertex
fn vs_main(
    vertex: VertexInput,
    instance: InstanceInput,
) -> VertexOutput {
    var out: VertexOutput;


    var transform_mat = mat2x2<f32>(instance.transform.x, instance.transform.y,instance.transform.z,instance.transform.w);

    out.tex_coords = (transform_mat * (vertex.tex_coords - 0.5)+ 0.5);
    out.texture_position = instance.texture_position;
    out.texture_size = instance.texture_size;
    out.color = instance.color;


    var pos: vec2<f32> = instance.position.xy + vertex.tex_coords * instance.size;

    out.clip_position = projection.view_proj * vec4<f32>(pos, instance.position.z, 1.0);
    return out;
}

// Fragment shader

@group(0) @binding(0)
var t_diffuse: texture_2d<f32>;
@group(0)@binding(1)
var s_diffuse: sampler;

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {

    let size : vec2<i32>= textureDimensions(t_diffuse, 0);
    let coord: vec2<f32> = (in.tex_coords * in.texture_size + in.texture_position) / vec2<f32>(size);

    var color = textureSample(t_diffuse, s_diffuse, coord);

    if color.a == 0.0 {
        discard;
    }

    return color * in.color;
}