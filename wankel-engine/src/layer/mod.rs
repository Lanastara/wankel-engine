use std::{any::TypeId, collections::HashMap};

use image::DynamicImage;
use wankel_engine_platform::{
    prelude::{Rect, Size},
    tracing::trace,
};
use wgpu::Color;

use crate::{
    asset::{AssetServer, Assets},
    render,
    texture::{
        atlas::{TextureAtlas, UniformTextureAtlasBuilder},
        Texture,
    },
    util::{HandleMap, Next, PartialOrdVec},
    Handle, HandleId, PrepareContext, RenderContext, ToAny,
};

use self::{text::TextSection, texture_atlas::Atlas};

pub mod text;
pub mod texture_atlas;

pub trait LayerConstructor {
    type Params;

    fn new(ctx: InitContext, params: Self::Params) -> Self;
}

pub trait Layer: 'static {
    fn prepare(&mut self, _context: PrepareContext) {}

    fn render_solids(&mut self, _context: RenderContext) -> Result<(), wgpu::SurfaceError> {
        Ok(())
    }

    fn render_translucents(&mut self, _context: RenderContext) -> Result<(), wgpu::SurfaceError> {
        Ok(())
    }

    fn cleanup(&mut self) {}
}

pub struct InitContext<'a> {
    pub device: &'a wgpu::Device,
    // pub config: &'a wgpu::SurfaceConfiguration,
    pub size: &'a Size<f32>,
    pub config: &'a wgpu::SurfaceConfiguration,
}

pub struct UpdateContext<'a> {
    asset_server: &'a mut AssetServer,
    assets: &'a Assets,
    texture_lookup: &'a mut HashMap<Handle<DynamicImage>, Handle<Texture>>,
    texture_atlases: &'a HashMap<Handle<Atlas>, Box<dyn TextureAtlas>>,
    pending_atlases: &'a mut Vec<(Handle<Atlas>, Handle<Texture>, UniformTextureAtlasBuilder)>,
    last_texture_handle: &'a mut HandleId,
    background_color: &'a mut Color,
    drawing_context: &'a render::Context,
}

impl<T> Handle<T>
where
    T: Layer,
{
    pub fn update<TOut, TFn: FnMut(&mut T, &mut UpdateContext) -> TOut>(
        &self,
        ctx: &mut crate::Context,
        mut update: TFn,
    ) -> Option<TOut> {
        let crate::Context {
            layers,
            asset_server,
            assets,
            texture_lookup,
            texture_atlases,
            pending_atlases,
            last_texture_handle,
            background_color,
            drawing_context,
            ..
        } = ctx;

        let layer = layers.get_mut::<T>(self)?;

        let mut u = UpdateContext {
            asset_server: asset_server,
            assets: assets,
            texture_lookup: texture_lookup,
            texture_atlases: texture_atlases,
            pending_atlases: pending_atlases,
            last_texture_handle: last_texture_handle,
            background_color: background_color,
            drawing_context: drawing_context,
        };

        Some(update(layer, &mut u))
    }

    pub fn drop(&self, ctx: &mut crate::Context) {
        ctx.layers.remove(&self);
    }
}

impl Next for usize {
    fn next(&self) -> Self {
        self + 1
    }
}

pub(crate) trait AnyLayer: ToAny + Layer {}

impl<T> AnyLayer for T where T: ToAny + Layer {}

pub(crate) struct Layers(HandleMap<Box<dyn AnyLayer>, usize>);

impl Layers {
    pub(crate) fn new() -> Self {
        Self(HandleMap::default())
    }

    pub(crate) fn get_mut<THandle: Layer + 'static>(
        &mut self,
        handle: &Handle<THandle>,
    ) -> Option<&mut THandle> {
        let boxed = self.0.get_mut(&handle.0)?;

        let a = boxed.to_any_mut();

        let d = a.downcast_mut();

        d
    }

    pub(crate) fn get_raw_mut(&mut self, handle: &usize) -> Option<&mut dyn AnyLayer> {
        let boxed = self.0.get_mut(handle)?;

        Some(boxed.as_mut())
    }

    pub(crate) fn create<TLayer: 'static + AnyLayer + LayerConstructor>(
        &mut self,
        ctx: InitContext,
        params: TLayer::Params,
    ) -> Handle<TLayer> {
        let layer = TLayer::new(ctx, params);
        let boxed: Box<dyn AnyLayer> = Box::new(layer);

        Handle::<TLayer>(self.0.insert(boxed), Default::default())
    }

    pub(crate) fn remove<THandle: Layer + 'static>(&mut self, handle: &Handle<THandle>) {
        self.0.remove(&handle.0);
    }
}

pub(crate) struct LayerStack(PartialOrdVec<(Option<f32>, usize)>);

impl LayerStack {
    pub fn new() -> Self {
        Self(PartialOrdVec::new())
    }

    pub fn iter(&self) -> impl Iterator<Item = &(Option<f32>, usize)> {
        self.0.iter()
    }

    pub(crate) fn clear(&mut self) {
        self.0.clear()
    }

    pub(crate) fn insert(&mut self, item: (Option<f32>, usize)) {
        self.0.insert_ordered(item, |(o, _)| o)
    }

    // pub(crate) fn len(&self) -> usize {
    //     self.0.len()
    // }
}
