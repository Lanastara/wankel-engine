use std::{collections::HashMap, hash::Hash, marker::PhantomData};

use layer::{texture_atlas::Atlas, InitContext, Layer, LayerConstructor, LayerStack, Layers};
pub use wgpu_glyph::HorizontalAlign;

use platform::net::Websocket;
use tracing::trace;
use wankel_engine_platform::{
    geom::Size,
    net::{Message, WebsocketResult},
    Config,
};
use winit::{
    event::{Event, WindowEvent},
    event_loop::ControlFlow,
};
use winit_input_helper::WinitInputHelper as Input;

pub use bytemuck;
use image::DynamicImage;
pub use wankel_engine_macros;
pub use wankel_engine_platform::net;
pub use wankel_engine_platform::tracing;
pub use wgpu;
pub use wgpu::Color;
pub use winit;
pub use winit_input_helper;

#[cfg(not(target_arch = "wasm32"))]
pub use wankel_engine_native as platform;

#[cfg(target_arch = "wasm32")]
pub use wankel_engine_wasm as platform;

#[cfg(not(target_arch = "wasm32"))]
pub use tokio;

pub mod asset;
pub mod layer;
pub mod render;
pub mod texture;
pub mod util;

use asset::{Asset, AssetServer, Assets};
use texture::atlas::{TextureAtlas, UniformTextureAtlasBuilder};
use util::HandleMap;

pub mod prelude {
    pub use crate::asset::Font;
    pub use crate::layer::texture_atlas::prelude::*;
    pub use crate::texture::atlas::UniformTextureAtlas;
    pub use crate::{Handle, NineSlice, State, TextureOptions, TextureRotate};

    pub use image::DynamicImage;
    pub use wankel_engine_macros::main;
    pub use wankel_engine_platform::prelude::*;
    pub use wgpu::Color;
}

#[cfg(target_arch = "wasm32")]
pub use wasm_bindgen;
#[cfg(target_arch = "wasm32")]
pub use wasm_bindgen_futures;

#[cfg(target_arch = "wasm32")]
pub fn init() {
    console_error_panic_hook::set_once();
    let config = tracing_wasm::WASMLayerConfigBuilder::new()
        .set_max_level(if cfg!(debug_assertions) {
            tracing::Level::TRACE
        } else {
            tracing::Level::INFO
        })
        .build();

    tracing_wasm::set_as_global_default_with_config(config);
}

#[cfg(not(target_arch = "wasm32"))]
pub fn init() {
    let subscriber = tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(if cfg!(debug_assertions) {
            tracing::Level::TRACE
        } else {
            tracing::Level::INFO
        })
        // .with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
        .finish();

    tracing::subscriber::set_global_default(subscriber).unwrap();
}

pub trait ToAny {
    fn to_any(&self) -> &dyn std::any::Any;
    fn to_any_mut(&mut self) -> &mut dyn std::any::Any;
}

pub trait State {
    fn init(ctx: &mut Context) -> Self;

    fn update(&mut self, ctx: &mut Context, d_time: f32, io: &mut IO);
}

macro_rules! buffer_element_impl {
    ($ty:ty, $vertex_ty: expr) => {
        impl BufferElement for $ty {
            const VERTEX_FORMAT: ::wgpu::VertexFormat = $vertex_ty;
        }
    };
}

pub trait BufferElement {
    const VERTEX_FORMAT: wgpu::VertexFormat;
}

buffer_element_impl!(f32, wgpu::VertexFormat::Float32);
buffer_element_impl!([f32; 2], wgpu::VertexFormat::Float32x2);
buffer_element_impl!([f32; 3], wgpu::VertexFormat::Float32x3);
buffer_element_impl!([f32; 4], wgpu::VertexFormat::Float32x4);
buffer_element_impl!([[f32; 2]; 2], wgpu::VertexFormat::Float32x4);

buffer_element_impl!(f64, wgpu::VertexFormat::Float64);
buffer_element_impl!([f64; 2], wgpu::VertexFormat::Float64x2);
buffer_element_impl!([f64; 3], wgpu::VertexFormat::Float64x3);
buffer_element_impl!([f64; 4], wgpu::VertexFormat::Float64x4);
buffer_element_impl!([[f64; 2]; 2], wgpu::VertexFormat::Float64x4);

buffer_element_impl!([u8; 2], wgpu::VertexFormat::Uint8x2);
buffer_element_impl!([u8; 4], wgpu::VertexFormat::Uint8x4);
buffer_element_impl!([[u8; 2]; 2], wgpu::VertexFormat::Uint8x4);

buffer_element_impl!([i8; 2], wgpu::VertexFormat::Sint8x2);
buffer_element_impl!([i8; 4], wgpu::VertexFormat::Sint8x4);
buffer_element_impl!([[i8; 2]; 2], wgpu::VertexFormat::Sint8x4);

buffer_element_impl!(u32, wgpu::VertexFormat::Uint32);
buffer_element_impl!([u32; 2], wgpu::VertexFormat::Uint32x2);
buffer_element_impl!([u32; 3], wgpu::VertexFormat::Uint32x3);
buffer_element_impl!([u32; 4], wgpu::VertexFormat::Uint32x4);
buffer_element_impl!([[u32; 2]; 2], wgpu::VertexFormat::Uint32x4);

buffer_element_impl!(i32, wgpu::VertexFormat::Sint32);
buffer_element_impl!([i32; 2], wgpu::VertexFormat::Sint32x2);
buffer_element_impl!([i32; 3], wgpu::VertexFormat::Sint32x3);
buffer_element_impl!([i32; 4], wgpu::VertexFormat::Sint32x4);
buffer_element_impl!([[i32; 2]; 2], wgpu::VertexFormat::Sint32x4);

pub trait Buffer {
    fn desc<'a>() -> wgpu::VertexBufferLayout<'a>;
}

#[derive(Default)]
pub struct IO<'a> {
    input: Input,
    sockets: HandleMap<Websocket>,
    events: Vec<WindowEvent<'a>>,
}

impl<'a> IO<'a> {
    pub fn input(&self) -> &Input {
        &self.input
    }

    pub fn events(&'a self) -> &Vec<WindowEvent<'a>> {
        &self.events
    }

    pub fn send_to(
        &mut self,
        handle: Handle<Websocket>,
        message: Message,
    ) -> Option<Result<(), wankel_engine_platform::Error>> {
        let socket = self.sockets.get_mut(&handle)?;
        Some(socket.send_to(message))
    }

    pub fn get_messages(&mut self, handle: Handle<Websocket>) -> Option<Vec<WebsocketResult>> {
        let socket = self.sockets.get_mut(&handle)?;
        Some(socket.get_messages())
    }

    pub fn connect_to(
        &mut self,
        url: &str,
    ) -> Result<Handle<Websocket>, wankel_engine_platform::Error> {
        let websocket = Websocket::new(url)?;
        Ok(self.sockets.insert(websocket))
    }
}

pub struct Context<'a> {
    layers: &'a mut Layers,
    layer_stack: &'a mut LayerStack,
    asset_server: &'a mut AssetServer,
    assets: &'a Assets,
    texture_lookup: &'a mut HashMap<Handle<DynamicImage>, Handle<texture::Texture>>,
    texture_atlases: &'a HashMap<Handle<Atlas>, Box<dyn TextureAtlas>>,
    pending_atlases: &'a mut Vec<(
        Handle<Atlas>,
        Handle<texture::Texture>,
        UniformTextureAtlasBuilder,
    )>,
    last_texture_handle: &'a mut HandleId,
    background_color: &'a mut Color,
    drawing_context: &'a render::Context,
}

impl<'a> Context<'a> {
    pub fn size(&self) -> Size<u32> {
        Size::new(
            self.drawing_context.size.width,
            self.drawing_context.size.height,
        )
    }

    fn next_texture_handle(&mut self) -> Handle<texture::Texture> {
        let next = *self.last_texture_handle + 1;

        *self.last_texture_handle = next;

        Handle::new(next)
    }

    pub fn upload_texture(&mut self, handle: &Handle<DynamicImage>) -> Handle<texture::Texture> {
        if let Some(handle) = self.texture_lookup.get(handle) {
            *handle
        } else {
            let t_handle = self.next_texture_handle();
            self.texture_lookup.insert(handle.clone(), t_handle);
            t_handle
        }
    }

    pub fn create_atlas(&mut self, builder: UniformTextureAtlasBuilder) -> Handle<Atlas> {
        let texture_handle = self.next_texture_handle();
        let handle = Handle::new(self.texture_atlases.len() + self.pending_atlases.len());
        self.pending_atlases.push((handle, texture_handle, builder));

        handle
    }

    pub fn push_layer<T: Layer>(&mut self, depth: Option<f32>, layer: Handle<T>) {
        self.layer_stack.insert((depth, layer.0));
    }

    pub fn background_color(&mut self, color: Color) {
        *self.background_color = color;
    }

    pub fn load_asset<T: Asset>(&mut self, path: &str) -> Handle<T> {
        self.asset_server.load(path)
    }

    pub fn create_layer<T: 'static + Layer + LayerConstructor + ToAny>(
        &mut self,
        params: T::Params,
    ) -> Handle<T> {
        let ctx = InitContext {
            config: &self.drawing_context.config,
            device: &self.drawing_context.device,
            size: &Size::new(
                self.drawing_context.size.width as f32,
                self.drawing_context.size.height as f32,
            ),
        };
        self.layers.create::<T>(ctx, params)
    }
}

fn get_color_rgba(color: &Color) -> [f32; 4] {
    [
        color.r as f32,
        color.g as f32,
        color.b as f32,
        color.a as f32,
    ]
}

#[derive(Debug, Clone)]
pub struct NineSlice {
    textures: [[Handle<DynamicImage>; 3]; 3],
    border_width: f32,
}

impl NineSlice {
    pub fn new(textures: [[Handle<DynamicImage>; 3]; 3], border_width: f32) -> Self {
        Self {
            border_width,
            textures,
        }
    }

    pub fn fill_texture_atlas(&self, builder: &mut texture::atlas::UniformTextureAtlasBuilder) {
        builder
            .add_texture(self.textures[0][0])
            .add_texture(self.textures[0][1])
            .add_texture(self.textures[0][2])
            .add_texture(self.textures[1][0])
            .add_texture(self.textures[1][1])
            .add_texture(self.textures[1][2])
            .add_texture(self.textures[2][0])
            .add_texture(self.textures[2][1])
            .add_texture(self.textures[2][2]);
    }
}

pub struct PrepareContext<'a> {
    pub texture_atlases: &'a mut HashMap<Handle<Atlas>, Box<dyn TextureAtlas>>,
    pub textures: &'a mut HashMap<Handle<texture::Texture>, texture::Texture>,
    pub assets: &'a Assets,
    pub size: &'a Size<f32>,
    pub d_time: f32,
    pub device: &'a wgpu::Device,
    pub config: &'a wgpu::SurfaceConfiguration,
    pub queue: &'a wgpu::Queue,
    pub depth_texture: &'a texture::Texture,
    pub depth: f32,
}

pub struct RenderContext<'a> {
    pub textures: &'a HashMap<Handle<texture::Texture>, texture::Texture>,
    pub size: &'a Size<f32>,
    pub device: &'a wgpu::Device,
    pub config: &'a wgpu::SurfaceConfiguration,
    pub encoder: &'a mut wgpu::CommandEncoder,
    pub view: &'a wgpu::TextureView,
    pub depth_texture: &'a texture::Texture,
}

#[derive(Debug, Clone)]
pub enum TextureRotate {
    Rotate0,
    Rotate90,
    Rotate180,
    Rotate270,
}

impl TextureRotate {
    fn get_matrix(&self) -> cgmath::Matrix2<f32> {
        match self {
            TextureRotate::Rotate0 => cgmath::Matrix2::new(1.0, 0.0, 0.0, 1.0),
            TextureRotate::Rotate90 => cgmath::Matrix2::new(0.0, -1.0, 1.0, 0.0),
            TextureRotate::Rotate180 => cgmath::Matrix2::new(-1.0, 0.0, 0.0, -1.0),
            TextureRotate::Rotate270 => cgmath::Matrix2::new(0.0, 1.0, -1.0, 0.0),
        }
    }
}

impl Default for TextureRotate {
    fn default() -> Self {
        Self::Rotate0
    }
}

#[derive(Debug, Clone, Default)]
pub struct TextureOptions {
    pub tint_color: Option<Color>,
    pub flip_h: bool,
    pub flip_v: bool,
    pub rotate: TextureRotate,
    pub disabled: bool,
    pub specific: SpecificTextureOptions,
}

#[derive(Debug, Clone)]
pub enum SpecificTextureOptions {
    Solid(SolidTextureOptions),
    Translucent(TranslucentTextureOptions),
}

impl Default for SpecificTextureOptions {
    fn default() -> Self {
        Self::Solid(Default::default())
    }
}

#[derive(Debug, Clone, Default)]
pub struct SolidTextureOptions {
    depth_overwrite: Option<f32>,
}

#[derive(Debug, Clone)]
pub struct TranslucentTextureOptions {
    Opacity: f32,
}

impl Default for TranslucentTextureOptions {
    fn default() -> Self {
        Self { Opacity: 1.0 }
    }
}

impl TextureOptions {
    fn get_transform(&self) -> [[f32; 2]; 2] {
        let identity = cgmath::Matrix2::new(1.0, 0.0, 0.0, 1.0);

        let m = self.rotate.get_matrix()
            * if self.flip_h {
                cgmath::Matrix2::new(-1.0, 0.0, 0.0, 1.0)
            } else {
                identity.clone()
            }
            * if self.flip_v {
                cgmath::Matrix2::new(1.0, 0.0, 0.0, -1.0)
            } else {
                identity
            };
        let a: [[f32; 2]; 2] = m.into();
        a
    }

    fn get_color(&self) -> [f32; 4] {
        get_color_rgba(&self.tint_color.unwrap_or(Color::WHITE))
    }
}

type HandleId = usize;

pub struct Handle<TAsset, THandle = HandleId>(THandle, PhantomData<TAsset>);

impl<TAsset, THandle: Default> Default for Handle<TAsset, THandle> {
    fn default() -> Self {
        Self(Default::default(), Default::default())
    }
}

impl<T, THandle: util::Next> util::Next for Handle<T, THandle> {
    fn next(&self) -> Handle<T, THandle> {
        Handle(self.0.next(), Default::default())
    }
}

impl<TAsset, THandle: Default> Handle<TAsset, THandle> {
    pub(crate) fn none() -> Self {
        Self(Default::default(), PhantomData::default())
    }

    pub(crate) fn new(id: THandle) -> Self {
        Self(id, PhantomData::default())
    }
}

impl<T, THandle: std::fmt::Debug> std::fmt::Debug for Handle<T, THandle> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        std::fmt::Debug::fmt(&self.0, f)
    }
}

impl<T, THandle: PartialEq> PartialEq for Handle<T, THandle> {
    fn eq(&self, other: &Self) -> bool {
        self.0.eq(&other.0)
    }
}

impl<T, THandle: Clone> Clone for Handle<T, THandle> {
    fn clone(&self) -> Self {
        Self(self.0.clone(), PhantomData::default())
    }
}

impl<T, THandle: Copy> Copy for Handle<T, THandle> {}

impl<T, THandle: Eq> Eq for Handle<T, THandle> {}

impl<T, THandle: Hash> Hash for Handle<T, THandle> {
    fn hash<H: std::hash::Hasher>(&self, state: &mut H) {
        self.0.hash(state)
    }
}

pub async fn run<T>(config: Config)
where
    T: 'static + State,
{
    let mut io = IO::default();
    trace!("IO created");

    let assets = Assets::default();
    let mut builder = AssetServer::builder();

    builder.register_loader(asset::ImageLoader);
    builder.register_loader(asset::FontLoader);

    let mut asset_server = builder.build(assets.clone(), platform::io::IO);

    let mut layers = Layers::new();
    let mut textures = HashMap::new();
    let mut texture_lookup = HashMap::new();
    let mut texture_atlases = HashMap::new();
    let mut background_color = Color::BLACK;
    let mut layer_stack = LayerStack::new();
    let mut pending_atlases = Vec::new();
    let mut last_texture_handle = 0;

    trace!("Context created");

    let mut last_frame = instant::Instant::now();

    let window_size = winit::dpi::PhysicalSize::new(config.size.width, config.size.height);

    let event_loop = winit::event_loop::EventLoop::new();
    trace!("Eventloop created");

    let window = winit::window::WindowBuilder::new()
        .with_title(&config.title)
        .with_inner_size(window_size)
        .build(&event_loop)
        .unwrap();
    trace!("window created");

    #[cfg(target_arch = "wasm32")]
    {
        use winit::platform::web::WindowExtWebSys;
        let canvas = web_sys::Element::from(window.canvas());

        let sys_window = web_sys::window().unwrap();
        let document = sys_window.document().unwrap();
        document.set_title(&config.title);
        let body = document.body().unwrap();

        body.append_child(&canvas)
            .expect("Append canvas to HTML body");
        trace!("Canvas created");
    };

    let mut drawing_context = render::Context::new(window, window_size).await;

    trace!("DrawingContext created");

    let mut state = {
        let mut context = Context {
            layers: &mut layers,
            layer_stack: &mut layer_stack,
            asset_server: &mut asset_server,
            assets: &assets,
            texture_lookup: &mut texture_lookup,
            texture_atlases: &texture_atlases,
            pending_atlases: &mut pending_atlases,
            last_texture_handle: &mut last_texture_handle,
            background_color: &mut background_color,
            drawing_context: &drawing_context,
        };
        T::init(&mut context)
    };
    trace!("State created");

    drawing_context.window.request_redraw();

    event_loop.run(move |event, _target, control_flow| {
        // trace!(?event);

        *control_flow = ControlFlow::Poll;

        if io.input.update(&event) {
            asset_server.update();
            let frame = instant::Instant::now();

            let dur = (frame - last_frame).as_secs_f32();

            {
                let mut context = Context {
                    layers: &mut layers,
                    layer_stack: &mut layer_stack,
                    asset_server: &mut asset_server,
                    assets: &assets,
                    texture_lookup: &mut texture_lookup,
                    texture_atlases: &texture_atlases,
                    pending_atlases: &mut pending_atlases,
                    last_texture_handle: &mut last_texture_handle,
                    background_color: &mut background_color,
                    drawing_context: &drawing_context,
                };

                state.update(&mut context, dur, &mut io);
            }

            {
                let mut context = render::PrepareContext {
                    d_time: dur,
                    assets: &assets,
                    texture_lookup: &mut texture_lookup,
                    textures: &mut textures,
                    texture_atlases: &mut texture_atlases,
                    pending_atlases: &mut pending_atlases,
                    layers: &mut layers,
                    layers_stack2: &layer_stack,
                };
                drawing_context.prepare(&mut context);
            }

            last_frame = frame;

            {
                let mut context = render::RenderContext {
                    background_color: &mut background_color,
                    layers: &mut layers,
                    layers_stack2: &layer_stack,
                    textures: &textures,
                };
                match drawing_context.render(&mut context) {
                    Ok(_) => {}
                    // Reconfigure the surface if lost
                    Err(wgpu::SurfaceError::Lost) => drawing_context.resize(drawing_context.size),
                    // The system is out of memory, we should probably quit
                    Err(wgpu::SurfaceError::OutOfMemory) => *control_flow = ControlFlow::Exit,
                    // All other errors (Outdated, Timeout) should be resolved by the next frame
                    Err(e) => eprintln!("{:?}", e),
                }
            }

            drawing_context.cleanup(&layer_stack, &mut layers);

            layer_stack.clear();
            io.events.clear();

            drawing_context.window.request_redraw();
        }

        if io.input.quit() {
            *control_flow = ControlFlow::Exit
        }

        if let Some(size) = io.input.window_resized() {
            drawing_context.resize(size);
        }

        if let Event::WindowEvent { event, .. } = event {
            let s = event.to_static();

            if let Some(s) = s {
                io.events.push(s);
            }
        }
    });
}
