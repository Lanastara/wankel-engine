use std::{
    cmp::Ordering,
    collections::{HashMap, VecDeque},
    hash::Hash,
};

use crate::Handle;

#[derive(Debug)]
pub struct HandleMap<TValue, THandle = Handle<TValue>> {
    last_handle: THandle,
    map: HashMap<THandle, TValue>,
}

pub trait Next {
    fn next(&self) -> Self;
}

impl<TValue, THandle: Default> Default for HandleMap<TValue, THandle> {
    fn default() -> Self {
        Self {
            last_handle: Default::default(),
            map: Default::default(),
        }
    }
}

impl<TValue, THandle: Hash + Eq + Next + Clone> HandleMap<TValue, THandle> {
    pub fn get(&self, handle: &THandle) -> Option<&TValue> {
        self.map.get(&handle)
    }

    pub fn get_mut(&mut self, handle: &THandle) -> Option<&mut TValue> {
        self.map.get_mut(&handle)
    }

    pub fn insert(&mut self, value: TValue) -> THandle {
        self.last_handle = self.last_handle.next();
        self.map.insert(self.last_handle.clone(), value);
        self.last_handle.clone()
    }

    pub fn insert_raw(&mut self, key: THandle, value: TValue) {
        self.map.insert(key, value);
    }

    pub fn remove(&mut self, handle: &THandle) -> Option<TValue> {
        self.map.remove(&handle)
    }
}

#[derive(Debug)]
pub struct PartialOrdVec<T>(VecDeque<T>);

impl<T> PartialOrdVec<T> {
    pub fn new() -> Self {
        Self(VecDeque::new())
    }

    pub fn iter_mut(&mut self) -> impl Iterator<Item = &mut T> + DoubleEndedIterator {
        self.0.iter_mut()
    }

    pub fn iter(&self) -> impl Iterator<Item = &T> + DoubleEndedIterator {
        self.0.iter()
    }

    pub fn clear(&mut self) {
        self.0.clear()
    }

    pub fn insert_ordered<TOrd: PartialOrd, TFn: Fn(&T) -> &TOrd>(
        &mut self,
        item: T,
        order_by: TFn,
    ) {
        let index = self.get_insert_index(&item, order_by);
        self.0.insert(index, item);
    }

    pub fn get_insert_index<TOrd: PartialOrd, TFn: Fn(&T) -> &TOrd>(
        &self,
        item: &T,
        order_by: TFn,
    ) -> usize {
        self.0
            .binary_search_by(|cmp| {
                if PartialOrd::gt(order_by(cmp), order_by(item)) {
                    Ordering::Greater
                } else {
                    Ordering::Less
                }
            })
            .unwrap_err()
    }

    pub fn len(&self) -> usize {
        self.0.len()
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn insert_two_nulls() {
        let mut v: PartialOrdVec<(Option<()>, i32)> = PartialOrdVec::new();
        v.insert_ordered((None, 1), |(i, _)| i);

        dbg!(&v);

        assert_eq!(v.get_insert_index(&(None, 2), |i| i), 1)
    }

    #[test]
    fn insert_at_start() {
        let mut v = PartialOrdVec::new();
        v.insert_ordered(None, |i| i);
        v.insert_ordered(Some(0.1), |i| i);
        v.insert_ordered(Some(0.05), |i| i);

        dbg!(&v);

        assert_eq!(v.get_insert_index(&None, |i| i), 1);
    }

    #[test]
    fn insert_at_middle() {
        let mut v = PartialOrdVec::new();
        v.insert_ordered(None, |i| i);
        v.insert_ordered(Some(1), |i| i);
        v.insert_ordered(Some(2), |i| i);

        dbg!(&v);

        assert_eq!(v.get_insert_index(&Some(1), |i| i), 2)
    }

    #[test]
    fn insert_at_end() {
        let mut v = PartialOrdVec::new();
        v.insert_ordered(None, |i| i);
        v.insert_ordered(Some(1), |i| i);
        v.insert_ordered(Some(2), |i| i);

        dbg!(&v);

        assert_eq!(v.get_insert_index(&Some(4), |i| i), 3)
    }
}
