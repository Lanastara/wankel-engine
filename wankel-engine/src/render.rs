use std::{collections::HashMap};

use image::DynamicImage;
use wankel_engine_platform::tracing::{error, warn};
use wgpu::Color;

use crate::{
    asset::{Assets, LoadState},
    layer::{texture_atlas::Atlas, LayerStack, Layers},
    texture::{
        atlas::{TextureAtlas, UniformTextureAtlasBuilder},
        Texture,
    },
    Handle,
};

pub struct Context {
    pub(crate) surface: wgpu::Surface,
    pub(crate) device: wgpu::Device,
    pub(crate) queue: wgpu::Queue,
    pub(crate) config: wgpu::SurfaceConfiguration,
    pub(crate) window: winit::window::Window,
    pub(crate) size: winit::dpi::PhysicalSize<u32>,
    pub(crate) depth_texture: Texture,
}

pub(crate) struct PrepareContext<'a> {
    pub(crate) d_time: f32,
    pub(crate) assets: &'a Assets,
    pub(crate) texture_lookup: &'a mut HashMap<Handle<DynamicImage>, Handle<Texture>>,
    pub(crate) textures: &'a mut HashMap<Handle<Texture>, Texture>,
    pub(crate) texture_atlases: &'a mut HashMap<Handle<Atlas>, Box<dyn TextureAtlas>>,
    pub(crate) pending_atlases:
        &'a mut Vec<(Handle<Atlas>, Handle<Texture>, UniformTextureAtlasBuilder)>,
    pub(crate) layers: &'a mut Layers,
    pub(crate) layers_stack2: &'a LayerStack,
}

pub(crate) struct RenderContext<'a> {
    pub(crate) background_color: &'a Color,
    pub(crate) textures: &'a HashMap<Handle<Texture>, Texture>,
    pub(crate) layers: &'a mut Layers,
    pub(crate) layers_stack2: &'a LayerStack,
}

impl Context {
    pub(crate) async fn new(
        window: winit::window::Window,
        size: winit::dpi::PhysicalSize<u32>,
    ) -> Self {
        let instance = wgpu::Instance::new(wgpu::Backends::all());
        let surface = unsafe { instance.create_surface(&window) };
        let adapter = instance
            .request_adapter(&wgpu::RequestAdapterOptions {
                power_preference: wgpu::PowerPreference::HighPerformance,
                compatible_surface: Some(&surface),
                force_fallback_adapter: false,
            })
            .await
            .unwrap();
        let (device, queue) = adapter
            .request_device(
                &wgpu::DeviceDescriptor {
                    label: None,
                    features: wgpu::Features::empty(),
                    // WebGL doesn't support all of wgpu's features, so if
                    // we're building for the web we'll have to disable some.
                    limits: if cfg!(target_arch = "wasm32") {
                        wgpu::Limits::downlevel_webgl2_defaults()
                    } else {
                        wgpu::Limits::default()
                    },
                },
                // Some(&std::path::Path::new("trace")), // Trace path
                None,
            )
            .await
            .unwrap();
        let config = wgpu::SurfaceConfiguration {
            usage: wgpu::TextureUsages::RENDER_ATTACHMENT,
            format: surface.get_supported_formats(&adapter)[0],
            width: size.width,
            height: size.height,
            present_mode: wgpu::PresentMode::AutoNoVsync,
            alpha_mode: wgpu::CompositeAlphaMode::Auto,
        };
        surface.configure(&device, &config);

        let depth_texture = Texture::create_depth(&config, &device);

        Context {
            config,
            device,
            queue,
            surface,
            window,
            size,
            depth_texture,
        }
    }

    pub(crate) fn prepare(&mut self, ctx: &mut PrepareContext) -> () {
        let PrepareContext {
            d_time,
            assets,
            texture_lookup,
            textures,
            texture_atlases,
            pending_atlases,
            layers,
            layers_stack2,
        } = ctx;

        let keys: Vec<_> = texture_lookup.keys().cloned().collect();

        for handle in keys {
            let tex = texture_lookup.get(&handle).unwrap().clone();

            if !textures.contains_key(&tex) {
                if let Some(lock) = assets.get(&handle) {
                    let state: &LoadState<_> = &lock;

                    if let LoadState::Loaded(img) = state {
                        let img = img.to_rgba8();

                        textures.insert(
                            tex,
                            Texture::create_and_upload(&self.device, &self.queue, img),
                        );

                        texture_lookup.remove(&handle);
                    } else {
                        warn!("texture not ready");
                    }
                } else {
                    warn!("texture not found");
                }
            }
        }

        for (atlas_handle, texture_handle, atlas) in pending_atlases.drain(..) {
            let atlas = atlas.build(self, texture_handle, assets.clone());
            texture_atlases.insert(atlas_handle, Box::new(atlas));
        }

        for (_handle, atlas) in texture_atlases.iter_mut() {
            let handle = atlas.get_texture();
            if let Some(texture) = atlas.update(self) {
                textures.insert(handle, texture);
            }
        }

        let size = wankel_engine_platform::prelude::Size::new(
            self.size.width as f32,
            self.size.height as f32,
        );

        for (depth, handle) in layers_stack2.iter() {
            if let Some(layer) = layers.get_raw_mut(handle) {
                let prepare = crate::PrepareContext {
                    device: &self.device,
                    config: &self.config,
                    queue: &self.queue,
                    assets,
                    d_time: *d_time,
                    size: &size,
                    texture_atlases,
                    textures,
                    depth_texture: &self.depth_texture,
                    depth: depth.unwrap_or_default(),
                };

                layer.prepare(prepare);
            } else {
                error!("Could not prepare Layer {}", handle);
            }
        }
    }

    pub(crate) fn render(&self, ctx: &mut RenderContext) -> Result<(), wgpu::SurfaceError> {
        let RenderContext {
            background_color,
            textures,
            layers,
            layers_stack2,
        } = ctx;

        let output = self.surface.get_current_texture()?;
        let view = output
            .texture
            .create_view(&wgpu::TextureViewDescriptor::default());
        let mut encoder = self
            .device
            .create_command_encoder(&wgpu::CommandEncoderDescriptor {
                label: Some("Render Encoder"),
            });

        {
            let _render_pass = encoder.begin_render_pass(&wgpu::RenderPassDescriptor {
                label: Some("Clear Render Pass"),
                color_attachments: &[Some(wgpu::RenderPassColorAttachment {
                    view: &view,
                    resolve_target: None,
                    ops: wgpu::Operations {
                        load: wgpu::LoadOp::Clear(background_color.to_owned()),
                        store: true,
                    },
                })],
                depth_stencil_attachment: Some(wgpu::RenderPassDepthStencilAttachment {
                    view: &self.depth_texture.view,
                    depth_ops: Some(wgpu::Operations {
                        load: wgpu::LoadOp::Clear(1.0),
                        store: true,
                    }),
                    stencil_ops: None,
                }),
            });
        }

        let size = wankel_engine_platform::prelude::Size::new(
            self.size.width as f32,
            self.size.height as f32,
        );

        for (_depth, handle) in layers_stack2.iter() {
            if let Some(layer) = layers.get_raw_mut(handle) {
                let render = crate::RenderContext {
                    device: &self.device,
                    config: &self.config,
                    size: &size,
                    textures: &textures,
                    encoder: &mut encoder,
                    view: &view,
                    depth_texture: &self.depth_texture,
                };

                if let Err(err) = layer.render_solids(render){
                    error!("Error during rednering solids: {:?}", err)
                }
            } else {
                error!("Could not render Layer {} solids", handle);
            }
        }

        for (_depth, handle) in layers_stack2.iter() {
            if let Some(layer) = layers.get_raw_mut(handle) {
                let render = crate::RenderContext {
                    device: &self.device,
                    config: &self.config,
                    size: &size,
                    textures: &textures,
                    encoder: &mut encoder,
                    view: &view,
                    depth_texture: &self.depth_texture,
                };

                if let Err(err) = layer.render_translucents(render){
                    error!("Error during rednering solids: {:?}", err)
                }
            } else {
                error!("Could not render Layer {} translucents", handle);
            }
        }

        // submit will accept anything that implements IntoIter
        self.queue.submit(std::iter::once(encoder.finish()));
        output.present();

        Ok(())
    }

    pub(crate) fn cleanup(&mut self, layers_stack: &LayerStack, layers: &mut Layers) {
        for (_depth, handle) in layers_stack.iter() {
            if let Some(layer) = layers.get_raw_mut(handle) {
                layer.cleanup();
            }
        }
    }

    pub(crate) fn resize(&mut self, new_size: winit::dpi::PhysicalSize<u32>) {
        if new_size.width > 0 && new_size.height > 0 {
            self.size = new_size;
            self.config.width = new_size.width;
            self.config.height = new_size.height;
            self.surface.configure(&self.device, &self.config);
            self.depth_texture = Texture::create_depth(&self.config, &self.device);
        }
    }
}
