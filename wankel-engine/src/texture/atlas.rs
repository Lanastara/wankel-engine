use std::collections::{HashMap, HashSet};

use image::{DynamicImage, GenericImageView};
use wankel_engine_platform::tracing::{self, instrument, error, info};
use wankel_engine_platform::geom::Rect;

use crate::{
    asset::{Assets, LoadState},
    Handle,
};

use super::Texture;

pub trait TextureAtlas {
    fn get_texture_position(&self, handle: Handle<DynamicImage>) -> Option<Rect<u32>>;
    fn get_loaded_texture(&self, handle: Handle<DynamicImage>) -> Option<Rect<u32>>;
    fn update(&mut self, context: &mut crate::render::Context) -> Option<crate::texture::Texture>;
    fn get_texture(&self) -> Handle<Texture>;
}

pub struct UniformTextureAtlas {
    pending: HashSet<Handle<DynamicImage>>,
    tile_width: u32,
    tile_height: u32,
    assets: Assets,
    positions: HashMap<Handle<DynamicImage>, Rect<u32>>,
    texture: Option<crate::texture::Texture>,
    texture_handle: Handle<Texture>,
}

impl UniformTextureAtlas {
    pub fn builder(parameters: (u32, u32)) -> UniformTextureAtlasBuilder {
        UniformTextureAtlasBuilder::new(parameters)
    }

    fn new(
        pending: HashSet<Handle<DynamicImage>>,
        tile_width: u32,
        tile_height: u32,
        context: &mut crate::render::Context,
        texture_handle: Handle<Texture>,
        assets: Assets,
    ) -> Self {
        let num_pixels = pending.len() as f64 * tile_height as f64 * tile_width as f64;
        let tmp_len = num_pixels.sqrt() as u32;
        let px_width = tmp_len.next_power_of_two() as u32;
        let num_tile_width = px_width as u32 / tile_width;
        let num_tile_height = (pending.len() as f32 / num_tile_width as f32).ceil() as u32;
        let px_height = num_tile_height * tile_height as u32;
        let px_height = px_height.next_power_of_two();

        let mut positions = HashMap::new();

        for (index, handle) in pending.iter().enumerate() {
            positions.insert(
                *handle,
                Rect {
                    width: tile_width,
                    height: tile_height,
                    x: (index % num_tile_width as usize) as u32 * tile_width,
                    y: (index / num_tile_width as usize) as u32 * tile_height,
                },
            );
        }

        let texture = Some(Texture::create(&context.device, px_width, px_height));

        Self {
            tile_width,
            tile_height,
            pending,
            positions,
            texture,
            texture_handle,
            assets,
        }
    }
}

impl TextureAtlas for UniformTextureAtlas {
    fn get_loaded_texture(&self, handle: Handle<DynamicImage>) -> Option<Rect<u32>> {
        if self.pending.contains(&handle) {
            None
        } else {
            self.get_texture_position(handle)
        }
    }

    fn get_texture_position(&self, handle: Handle<DynamicImage>) -> Option<Rect<u32>> {
        self.positions.get(&handle).cloned()
    }

    fn get_texture(&self) -> Handle<Texture> {
        self.texture_handle
    }

    #[instrument(skip(self, context))]
    fn update(&mut self, context: &mut crate::render::Context) -> Option<crate::texture::Texture> {
        if !self.pending.is_empty() {
            let mut finished = HashSet::new();

            for handle in self.pending.iter() {
                if let Some(lock) = self.assets.get(handle) {
                    let state: &LoadState<_> = &lock;
                    match state {
                        LoadState::Loaded(img) => {
                            if let Some(pos) = self.positions.get(handle) {
                                if let Some(texture) = self.texture.as_mut() {
                                    match img.as_rgba8() {
                                        Some(rgba) => {
                                            let dimensions = img.dimensions();
                                            

                                            assert_eq!(dimensions.0, self.tile_width as u32);
                                            assert_eq!(dimensions.1, self.tile_height as u32);

                                            
                                            texture.upload(&context.queue, (pos.x, pos.y), (self.tile_width, self.tile_height), rgba.to_vec());

                                            

                                            finished.insert(*handle);
                                        }
                                        None => {
                                            error!("invalid pixel format");
                                        }
                                    };

                                    //let rgba = .unwrap();
                                } else {
                                    panic!()
                                }
                            }
                        }
                        _ => {}
                    }
                }
            }

            self.pending = self.pending.difference(&finished).cloned().collect();

            if self.pending.is_empty() {
                let texture = self.texture.take();

                if let Some(texture) = texture {
                    info!("TextureAtlas Loaded");
                    return Some(texture);
                }
            }
        }
        None
    }
}

pub struct UniformTextureAtlasBuilder {
    entries: HashSet<Handle<DynamicImage>>,
    tile_width: u32,
    tile_height: u32,
}

impl UniformTextureAtlasBuilder {
    fn new((tile_width, tile_height): (u32, u32)) -> Self {
        Self {
            tile_width,
            tile_height,
            entries: HashSet::new(),
        }
    }

    pub fn add_texture(&mut self, handle: Handle<DynamicImage>) -> &mut Self {
        self.entries.insert(handle);

        self
    }

    pub(crate) fn build(
        self,
        context: &mut crate::render::Context,
        texture_handle: Handle<Texture>,
        assets: Assets,
    ) -> UniformTextureAtlas {
        UniformTextureAtlas::new(
            self.entries,
            self.tile_height,
            self.tile_width,
            context,
            texture_handle,
            assets,
        )
    }
}
