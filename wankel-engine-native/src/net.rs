use std::{
    net::TcpStream,
    sync::{
        atomic::{AtomicBool, Ordering},
        mpsc::{channel, Receiver},
        Arc, Mutex,
    },
    thread::spawn,
    time::Duration,
};
use tungstenite::{stream::MaybeTlsStream, WebSocket};
use wankel_engine_platform::{
    net::{Message, WebsocketResult},
    Error,
};

pub struct Websocket {
    inner: Arc<Mutex<WebSocket<MaybeTlsStream<TcpStream>>>>,
    messages: Receiver<wankel_engine_platform::net::WebsocketResult>,
    cancel: Arc<AtomicBool>,
}

impl Websocket {
    pub fn new(url: &str) -> Result<Self, Error> {
        let (sender, receiver) = channel();
        let (mut socket, _response) =
            tungstenite::connect(url).map_err(|e| Error::new(&format!("{}", e)))?;

        let inner_socket = socket.get_mut();
        match inner_socket {
            &mut MaybeTlsStream::Plain(ref mut s) => {
                s.set_nonblocking(true)
                    .map_err(|_| Error::new("Could not set socket to non blocking"))?;
            }
            _ => {}
        };

        let inner = Arc::new(Mutex::new(socket));
        let cancel = Arc::new(AtomicBool::new(false));

        spawn({
            let cancel = cancel.clone();
            let inner = inner.clone();

            move || {
                while !cancel.load(Ordering::Acquire) {
                    let message = {
                        let mut lock = inner.lock().unwrap();

                        let message = lock.read_message();
                        match message {
                            Ok(tungstenite::Message::Binary(b)) => {
                                Some(Some(crate::net::WebsocketResult::Message(
                                    crate::net::Message::Binary(b),
                                )))
                            }

                            Ok(tungstenite::Message::Close(_)) => {
                                Some(Some(crate::net::WebsocketResult::Disconnect))
                            }
                            Ok(tungstenite::Message::Ping(_)) => None,
                            Ok(tungstenite::Message::Pong(_)) => None,
                            Ok(tungstenite::Message::Text(b)) => {
                                Some(Some(crate::net::WebsocketResult::Message(
                                    crate::net::Message::String(b),
                                )))
                            }
                            Err(tungstenite::Error::ConnectionClosed) => {
                                Some(Some(crate::net::WebsocketResult::Disconnect))
                            }
                            Err(tungstenite::Error::AlreadyClosed) => {
                                Some(Some(crate::net::WebsocketResult::Disconnect))
                            }
                            Err(tungstenite::Error::Io(ref ioe)) => match ioe.kind() {
                                std::io::ErrorKind::WouldBlock => Some(None),
                                _ => Some(Some(crate::net::WebsocketResult::Error(Error::new(
                                    "Error durin Communication",
                                )))),
                            },
                            Err(e) => Some(Some(crate::net::WebsocketResult::Error(Error::new(
                                &format!("{}", e),
                            )))),
                        }
                    };

                    match message {
                        Some(Some(message)) => {
                            sender.send(message).unwrap();
                        }
                        Some(None) => std::thread::sleep(Duration::from_millis(100)),
                        None => {}
                    }
                }
            }
        });

        Ok(Self {
            inner,
            messages: receiver,
            cancel: cancel,
        })
    }

    pub fn get_messages(&mut self) -> Vec<WebsocketResult> {
        let mut ret = Vec::new();
        loop {
            match self.messages.try_recv() {
                Ok(msg) => ret.push(msg),
                Err(std::sync::mpsc::TryRecvError::Empty) => break,
                Err(std::sync::mpsc::TryRecvError::Disconnected) => {
                    ret.push(WebsocketResult::Disconnect);
                    break;
                }
            }
        }

        ret
    }

    pub fn send_to(&mut self, message: Message) -> Result<(), Error> {
        let message = match message {
            Message::String(s) => tungstenite::Message::Text(s),
            Message::Binary(b) => tungstenite::Message::Binary(b),
        };

        let mut lock = self.inner.lock().unwrap();
        lock.write_message(message)
            .map_err(|e| Error::new(&format!("{}", e)))?;
        lock.write_pending()
            .map_err(|e| Error::new(&format!("{}", e)))?;

        Ok(())
    }

    pub fn close(&mut self) {
        self.cancel.store(true, Ordering::Release)
    }
}
