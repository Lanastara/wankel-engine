struct ProjectionUniform {
    view_proj: mat4x4<f32>,
};
@group(1) @binding(0)
var<uniform> projection: ProjectionUniform;

struct VertexInput {
    @location(0) depth: f32,
    @location(1) position: vec2<f32>,
    @location(2) uv: vec2<f32>,
    @location(3) color: vec4<f32>,
    @location(4) clip_min: vec2<f32>,
    @location(5) clip_max: vec2<f32>,
}

struct VertexOutput {
    @builtin(position) clip_position: vec4<f32>,
    @location(0) uv: vec2<f32>,
    @location(1) color: vec4<f32>,
    @location(2) clip_min: vec2<f32>,
    @location(3) clip_max: vec2<f32>,
}


@vertex
fn vs_main(
    vertex: VertexInput
) -> VertexOutput {

    var out: VertexOutput;

    out.clip_position = projection.view_proj * vec4(vertex.position, vertex.depth, 1.0);
    out.uv = vertex.uv;
    out.color = vertex.color;
    out.clip_min = vertex.clip_min;
    out.clip_max = vertex.clip_max;

    return out;
}

@group(0) @binding(0)
var t_diffuse: texture_2d<f32>;
@group(0)@binding(1)
var s_diffuse: sampler;

@fragment
fn fs_main(in: VertexOutput) -> @location(0) vec4<f32> {

    var color = textureSample(t_diffuse, s_diffuse, in.uv) * in.color;
    if(color.a == 0.0)
    {
        discard;
    }

    if(in.clip_position.x < in.clip_min.x)
    {
        discard;
    }

    if(in.clip_position.y < in.clip_min.y)
    {
        discard;
    }

    if(in.clip_position.x > in.clip_max.x)
    {
        discard;
    }

    if(in.clip_position.y > in.clip_max.y)
    {
        discard;
    }
    
    return color;
}