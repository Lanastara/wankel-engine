use std::cell::RefCell;
use std::rc::Rc;

pub use egui;
use egui::{
    epaint::ahash::{HashMap, HashMapExt},
    FullOutput, TextureId,
};
use egui_wgpu_backend::RenderPass;
use wankel_engine::{
    layer::{InitContext, Layer, LayerConstructor},
    texture::Texture,
    tracing::warn,
    wankel_engine_macros::Buffer,
    wgpu,
    winit::event::WindowEvent,
    Buffer, BufferElement, Handle,
};

// use wankel_engine::{cgmath, Handle, Layer, PipelineError, ShaderInterface};

mod events;

#[repr(C)]
#[derive(Clone, Copy, Debug, PartialEq, Buffer)]
struct Vertex {
    pub depth: f32,
    pub position: [f32; 2],
    pub uv: [f32; 2],
    pub color: [f32; 4],
    pub clip_min: [f32; 2],
    pub clip_max: [f32; 2],
}

impl
    From<(
        egui::epaint::Vertex,
        egui::Rect,
        &wankel_engine::prelude::Size<f32>,
    )> for Vertex
{
    fn from(
        (v, clip, size): (
            egui::epaint::Vertex,
            egui::Rect,
            &wankel_engine::prelude::Size<f32>,
        ),
    ) -> Self {
        let color = [
            v.color.r() as f32 / 255.0,
            v.color.g() as f32 / 255.0,
            v.color.b() as f32 / 255.0,
            v.color.a() as f32 / 255.0,
        ];

        Self {
            depth: 0.0, //TODO
            position: v.pos.into(),
            uv: v.uv.into(),
            color: color,
            clip_min: [clip.min.x, size.height - clip.max.y],
            clip_max: [clip.max.x, size.height - clip.min.y],
        }
    }
}

#[derive(Default, Debug)]
pub struct EguiLayerCache(RefCell<EguiLayerCacheInner>, Vec<egui::Event>);

impl EguiLayerCache {
    pub fn queue_user_texture(&self, handle: Handle<wankel_engine::texture::Texture>) {
        self.0.borrow_mut().queue_user_texture(handle);
    }
}

struct EguiLayerCacheInner {
    cursor_position: egui::Pos2,
    modifiers: egui::Modifiers,
    ctx: egui::Context,
    time: f64,
    pixels_per_point: f32,
    user_texture_queue: Vec<Handle<wankel_engine::texture::Texture>>,
}

impl EguiLayerCacheInner {
    fn queue_user_texture(&mut self, handle: Handle<wankel_engine::texture::Texture>) {
        self.user_texture_queue.push(handle);
    }
}

impl Default for EguiLayerCacheInner {
    fn default() -> Self {
        Self {
            cursor_position: Default::default(),
            modifiers: Default::default(),
            time: Default::default(),
            pixels_per_point: 1.0,
            ctx: Default::default(),
            user_texture_queue: Default::default(),
        }
    }
}

impl std::fmt::Debug for EguiLayerCacheInner {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        f.debug_struct("EguiLayerCacheInner")
            .field("cursor_position", &self.cursor_position)
            .field("modifiers", &self.modifiers)
            .field("time", &self.time)
            .field("pixels_per_point", &self.pixels_per_point)
            .finish()
    }
}

enum Output {
    Prepare(FullOutput),
    INVALIT,
    Render(
        Vec<egui::ClippedPrimitive>,
        egui_wgpu_backend::ScreenDescriptor,
        egui::TexturesDelta,
    ),
}

impl std::fmt::Debug for Output {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::Prepare(_) => write!(f, "Prepare"),
            Self::INVALIT => write!(f, "INVALIT"),
            Self::Render(_, _, _) => write!(f, "Render"),
        }
    }
}

impl Output {
    fn take(&mut self) -> Output {
        std::mem::replace(self, Output::INVALIT)
    }
}

pub struct EguiLayer {
    output: Output,
    cache: Rc<EguiLayerCache>,
    render_pass: egui_wgpu_backend::RenderPass,
    texture_lookup: HashMap<Handle<Texture>, egui::TextureId>,
}

impl EguiLayer {
    pub fn run<TFn: FnOnce(&egui::Context)>(
        &mut self,
        run_ui: TFn,
        size: wankel_engine::prelude::Size<u32>,
        events: &Vec<WindowEvent>,
    ) {
        let output = {
            let mut inner_cache = self.cache.0.borrow_mut();

            let mut raw_input = egui::RawInput::default();

            raw_input.screen_rect = Some(egui::Rect::from_min_size(
                egui::Pos2 { x: 0.0, y: 0.0 },
                egui::Vec2 {
                    x: size.width as f32,
                    y: size.height as f32,
                },
            ));
            raw_input.time = Some(inner_cache.time);

            for e in events.iter() {
                if let Some(e) = crate::events::get_egui_event(e, &mut inner_cache) {
                    raw_input.events.push(e);
                }
            }

            inner_cache.ctx.run(raw_input, run_ui)
        };

        self.output = Output::Prepare(output);
    }

    pub fn get_texture_id(&self, handle: &Handle<Texture>) -> Option<TextureId> {
        self.texture_lookup.get(&handle).cloned()
    }

    pub fn wants_pointer_input(&self) -> bool {
        self.cache.0.borrow().ctx.wants_pointer_input()
    }

    pub fn wants_keyboard_input(&self) -> bool {
        self.cache.0.borrow().ctx.wants_keyboard_input()
    }

    pub fn is_using_pointer(&self) -> bool {
        self.cache.0.borrow().ctx.is_using_pointer()
    }

    pub fn is_pointer_over_area(&self) -> bool {
        self.cache.0.borrow().ctx.is_pointer_over_area()
    }
}

impl LayerConstructor for EguiLayer {
    type Params = Rc<EguiLayerCache>;

    fn new(context: InitContext, cache: Rc<EguiLayerCache>) -> Self {
        Self {
            output: Output::INVALIT,
            cache,
            render_pass: RenderPass::new(context.device, context.config.format, 1),
            texture_lookup: HashMap::new(),
        }
    }
}

impl Layer for EguiLayer {
    fn prepare(&mut self, context: wankel_engine::PrepareContext) {
        let wankel_engine::PrepareContext {
            device,
            queue,
            textures,
            config,
            ..
        } = context;

        let mut cache = self.cache.0.borrow_mut();

        let mut i = 0;

        while i < cache.user_texture_queue.len() {
            if let Some(handle) = cache.user_texture_queue.get(i) {
                if let Some(texture) = textures.get(handle) {
                    self.texture_lookup.insert(
                        handle.clone(),
                        self.render_pass.egui_texture_from_wgpu_texture(
                            &device,
                            &texture.view,
                            wgpu::FilterMode::Nearest,
                        ),
                    );

                    cache.user_texture_queue.remove(i);
                } else {
                    i += 1;
                }
            }
        }

        if cache.user_texture_queue.is_empty() {
            if let Output::Prepare(output) = self.output.take() {
                let paint_jobs = cache.ctx.tessellate(output.shapes);

                let screen_descriptor = egui_wgpu_backend::ScreenDescriptor {
                    physical_width: config.width,
                    physical_height: config.height,
                    scale_factor: 1.0, //TODO window.scale_factor() as f32,
                };

                self.render_pass
                    .add_textures(&device, &queue, &output.textures_delta)
                    .unwrap();
                self.render_pass
                    .update_buffers(device, queue, &paint_jobs, &screen_descriptor);

                self.output = Output::Render(paint_jobs, screen_descriptor, output.textures_delta);
            } else {
                warn!("wrong output state: {:?}", self.output);
            }
        }
    }

    fn render_translucents(
        &mut self,
        context: wankel_engine::RenderContext,
    ) -> Result<(), wgpu::SurfaceError> {
        let cache = self.cache.0.borrow();
        if cache.user_texture_queue.is_empty() {
            let wankel_engine::RenderContext { encoder, view, .. } = context;

            if let Output::Render(paint_jobs, screen_descriptor, _tdelta) = &self.output {
                self.render_pass
                    .execute(encoder, view, &paint_jobs, &screen_descriptor, None)
                    .unwrap();
            } else {
                warn!("wrong output state: {:?}", self.output);
            }
        }

        Ok(())
    }

    fn cleanup(&mut self) {
        if let Output::Render(_paint_jobs, _screen_descriptor, tdelta) = self.output.take() {
            self.render_pass.remove_textures(tdelta).unwrap();
        }
    }
}
